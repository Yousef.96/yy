- Yousef Abdulla Saleh 195567
- Subsystem:Simulation
- Team:7

**Agenda**
- We had intended to complete the Ansys programme work in order to obtain the results of the aerodynamic characteristics based on the various angles that had been put up. The graph charting for the Lift vs Length and the Weight of Skin vs Length was then built.

**Goals**
- To determine the precise plots of the graph using the right analytical data. To determine the weight per skin of the airship, which is one of the missing factors ad callibration done from previous data submitted to Dr.

**Problem**
- Some required factors, such as the weight per skin of the airship, were lacking in order to determine the precise plots or trendline of the graph's curve. The weight per skin of the airship must be included in the weight per skin calculation to get the surface area.

**Decision**
- Decided to visit lab and took the reading on the weight per skin with the advice from Dr.

**Method**
- The weight per skin of the airship was calculated by folding the overall skin of the airship and handing it to the weight measurement scale with the rope.

**Justifications**
- A precise measuring intake can yield the exact weight per skin. The actual weight of the obtained skin may be incorporated in the calculations and further analysis on the charting of the graphs, which is the Weight of the skin versus Length.

**Impact**
- The lift vs length graph and the weight of skin vs length graph can be plotted correspondingly.

**Next Step**
- The correction will have to do unless there is the mistakes in the plots.
