**IDP Week4**

**Simulation**

**Name: Yousef Abdulla Saleh**

**Agenda**
Had designed the HAU airframe in the Computer Aided Designing tools like the CATIA V5. We had discussed the timeline of the simulation's progress in terms of the Real Time Measurements observing, data intake, Catia Programed Drawing or Integrated Designing, Performance Calculations of the HAU, Computational Fluid Dynamics Analysis (CFD), Ansys.


**Goals**
To design the airframe design of the HAU Airship with the help of the Computer Aided Tool, CATIA designing. Then, to perform the designing with the ratio selected as to have a good Computational Fluid Dynamics results in future progress.

**Problem**
Figured out that it is complicated to see the design of HAU through virtually, instead have to look for it in real time (Been faced in Week 2).Besides, the real time design the HAU airframe been observed is not enough to simulate the progress. Then, the CATIA could not able to process certain time when designing process took place.

**Decision**
To design the airframe of the airship with a simple notes of measurements.To design the Airframe with the CATIA programme or software.

**Method**
Sketched the design at first to have a good view imagination on the design itself.Began to design the model with the specific ratio of the measurements had been set up. Note: Tolerance on the measurements was set to avoid greater significant on the error involved.

**Justifications**
Will have a good presentation on the whole Airframe design of the HAU itself.To have a good theory or concept on the future design adjustmnent progress through the CATIA V5 programme.To have a good results on the Computational Fluid Dynamics (CFD) and performance results.

**Impact**
The design can be seen clearly and eel easy to noitice the curvature or the outline of the airframe compared to the real life design as it is bigger in size and hard to notice those minor outlines.Able to think and have a imagination as well as the plans to come out certain mmandatory parameters like the Lift, Drag, Thrust and Weight.

**Next Step**
The Computational Fluid Dynamics (CFD) process on the performnce of the airship had been planned out.
