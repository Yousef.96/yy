- Yousef Abdulla Saleh 195567
- Subsystem:Simulation
- Team:7

**Agenda**
- To complete the charting of the graph that was examined and revised a few days ago based on Dr's remarks to improve it.

**Goals**
- To obtain the correct plot of the curve line in the graph of Lift vs Length and Weight of Skin vs Length.

**Problem**
- A liltle bit of the curve error was found in the curve line of teh graph plotted.

**Decision**
- After numerous attempts to correct the inaccuracy from the plotted graph, I decided to present it to Dr Salah.

**Method**
- I had done the computations and re-analyzed the approach, as well as incorporated all of the weight-per-skin equations, which were previously missing.

**Justifications**
- The right plotting of the graph can be tracked down.

**Impact**
- The graph illustrates the relationship between the Lift and Weight per Skin vs. Length drawn with the appropriate curve line as needed.

**Next Step**
- To help the other teams in their work and also the feedback on the simulation task had been done.
