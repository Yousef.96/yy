- Yousef Abdulla Saleh 195567
- Subsystem:Simulation
- Team:7

**Agenda**
- To create the Ansys configuration for the planned airship with different angles. In addition, the graph plotting for the Lift vs Length and Weight of Skin vs Length graphs.

**Goals**
- To plot the graph of the relation by showing the curve trendline of the Lift vs Lenght and the Weight of skin vs Lenght graph.

**Problem**
- The issue was discovered while forming the correct equation in Excel to programme it. The graph has some errors in the curve fit, where the errors were discovered in the plot line, which is a zig-zag line.

**Decision**
- I opted to use references from a previous student's thesis book and documents. Then, I looked for the proper formula to get the final relationship where to draw the graph of the Lift and Weight of Skin that correspond to the length.

**Method**
- I had to go back and repeat the previous calculations with the correct formula that was so well integrated.

**Justifications**
- The plot of the Lift and weight per skin vss to the lenght can be plotted.

**Impact**
- The desired plots can be obtained as it is presentable.

**Next Step**
- The correction will have to do unless there is the mistakes in the plots.
