**Yousef Abdulla Saleh 195567**

**Subsystem:Simulation**

**Team:7**

**Agenda**
Had did the Ansys Programming Software Analysis on the steps findimng the CL, CD, Cd0 and Cm. Then, the graph which is volume vs weight and volume vs lift force was plotted.

**Goals**
To find out certain important parameters and do the calibrations to it. Especially, the Ansys results obtained on the  CL, CD, Cd0 and Cm. Then, continuing the calculations part on the finding the lenght of airship and certain parameters like the surface area, finnese ration and the relationship of the lenght with the lift force as well.

**Problem**
The Ansys programe software could not run on laptop due to heavy ram usage to generate the results. The steps on finding the way to calculate the parameters had mentioned above like the lenght of airship, finnese ratio, and the surface area.

**Decision**
The Ansys software analysis had been put under keep in view and without wasting time the progress had been moved on the parameters calculation which is mandatory to some out with the graph plotting.

**Method**
The calculations of the parameters like the Lenght of airship, finnese ratio and the parameters like the surface area had been refered to the points given by Dr. Salah as well as the help from the Fundamental of Airship Design Book.

**Justifications**
Able to present the relationship of lenght as weel with the lift force very well withn the graph plotting.

**Impact**
The progress would be go more details as expected on the certain mandatory parameters that had been missed out on the progress last week or it was unclear and now able to define it.

**Next Step**
The calibration process if taken place based on upcoming progress feedback on the Ansys programme results analysis as well as the calculations made on the parameters finding.

