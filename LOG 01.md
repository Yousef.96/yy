**logbook Entry Number:01**

1. What is the agenda this wek and what are my/our goals?
regarding Agenda

- To form out the Group Progress Gantt Chart.

- Discuss the simulation progress timeline (observing real-time measurements, data input, Catia Programed Drawing or Integrated Designing, HAU performance calculations, Computational Fluid Dynamics Analysis (CFD), Ansys).


2. As for the goals?
 Goal number one is to

- examine the HAU design in real time.

- Measurements are taken from the actual HAU simulated.

- Measurement selections in the top, side, and bottom views.

- Overall comprehensive physical HAU design measurements.


3. What decisions did you/your team make in solving a problem? 


- Decisions: we went to Lab H 2.1 to examine the HAU model and the real-time design.


4. How did you/your team make those decisions (method)? 

- Take accurate measurements of the HAU that is accessible.

- Measurements: Tolerance was chosen based on the measurements to minimise a larger impact on the mistake involved.


5.  Why did you/your team make that choice (justification) when solving the problem ?


- Create a prototype that appears decent in terms of the measurement ratio specified in the CATIA application.

- Conducting good performance calculations or assessments on the HAU model.

- To get satisfactory results using the Fluid Dynamics concept.

6. What was the impact on you/your team/the project when you make that decision ?

- Have good pictures or thoughts for designing the HAU in CATIA (Airframe Design).


- Consider the many metrics that may be involved (Lift, Drag, Thrust, Weight).
 
- Possible CFD results.


7.  What is the next step ?

- The design process will begin in the CATIA programme.
 
- It will be necessary to evaluate the parameters involved from other subsystem groups in order to have a better concept of performance calculations.
