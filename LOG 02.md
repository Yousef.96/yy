**IDP Week2**

Name:Yousef Abdulla Saleh 195567

Subsystem:Simulation

Team:7

**Agenda**
To form out the Group Progress Gantt Chart. Then, discuss the timeline of the progress in simulations (Real Time Measurements observing, data intake, Catia Programed Drawing or Integrat.

**Goals**
Looking the design of HAU in real time. The Measurements intake from the modelled exact HAU. Then, selections of the measurements in the Top, side, bottom view.Overall complete design measurements of physical HAU.

**Problem**
Figured out that it is complicated to see the design of HAU through virtually, instead have to look for it in real time. The measurements of real time HAU taken are not accurate due to systematic error and random error.

**Decision**
Took a visit to Lab H 2.1 to look at the HAU model and the real time design.


**Method**
Take the real measurements of the HAU which is available. Measurements: Tolerance was set according to the measurements to avoid greater significant on the error involved.

** Justifications**
To perform a prototype which looks good in terms of the measurements ratio set up in the CATIA programme. Then, to perform the good performance calculations or reviews on HAU model.To have a good results on the Fluid Dynamics concept.

** Impact**
Have a good illustrations or imaginations on how to design the HAU in the CATIA (Airframe Design). Besides, allow to think of the possibilities parameters involved (Lift, Drag, Thrust, Weight).Possible CFD outcomes.

**Next Step**
Designing process will begin in CATIA programme. Finally, have to review on the parameters involved from other subsystem group to have a better idea in performance calculations.
