Yousef Abdulla Saleh 195567
Subsystem:Simulation
Team:7

**Agenda**
To re-test the design using Computational Fluid Dynamics (CFD). To obtain data from the Ansys programme for the coefficient of lift (C l), coefficient of drag (c d), and moment coefficient (C m) parameters. Collecting weight data for each component and relating the volume of the airship to the total weight of the subsystems given.

**Goals**
To compute the relationship between the volume of the airship and the approximate weight accumulated by each subsystem. To carry out volume and weight relation-based proofs (calculations). To determine additional force characteristics such as the Bouyancy Force, Aerodynamic Lift, and Bouyancy Ratio.

**Problem**
Have issues with the volume and weight relationships of each system. I had difficulty making the calculations on the voiume and weight relationship. I can't visualise the lift or force from bouyancy idea and the aerodynamic qualities from the ANSYS simply.

**Decision**
To carry out the calculations in order to grasp the precise understanding of volume and weight relationships. Refer to the Fundamentals of Airship and Design book to determine the Bouyancy Lift, Bouyancy Ratio, and Aerodynamic Lift.

**Method**
Using the ellipsoid or spheroid volume theory, calculate the volume of the airship. Determine the total weight or gross weight of each tabulated subsystem. Please keep in mind that the total gross weight for this time period exceeds 10 kg because it is only an estimate. Once all of the subsystem weights have been finalised, the true gross weight will be presented.

**Justifications**
Capable of presenting the relationship between the volume and weight of each subsystem by charting the Volume versus Weight of Subsystem graph (Kg). Capable of calculating the estimated bouyancy lit, aerodynamic lift, and bouyancy ratio, BR.

**Impact**
The graph of Volume vs Weight of susbystem (Kg) had been genarated together the lift and bouyancy force had been known too.

**Next Step**
The formuation of relation between the lenght of airship and the lift force.
