Name:Yousef Abdlla Saleh 195567

Subsystem:**Simulation**

Team:7

**Agenda**
- Discuss with Fiqri on what parameters needed for simulation.
- Helping member on run simulation on the cad model.

**Goals**
- run a simulation for the HAU model.

**Decision to problems above**
- Using simulator to simulate the HAU.

**Method to solve the problems**
- Using ANSYS Fluent to simulate the HAU.

**Justification when solving problem **
- need to consider the skewness and orthogonal qualtiy during meshing process.
- To perform a good simulation.
- To have a good result on the Fluid Dynamics concept.

**Impact of/after the decision chosen**
- Think of the possibilities parameters involved (Lift, Drag, Thrust, Weight).
- Possible CFD outcomes.

**Next Step**
- run the simulation .
- need to find on the parameters needed (based on reference book).

