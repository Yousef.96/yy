**Yousef Abdulla Saleh 195567**

**Subsystem:Simulation**

**Team:7**

**Agenda**
We had planned out to do the calibration on the Ansys results and together with the calculation steps on finding out the major parameters like the lenght, a and the width, b that would be linked to the finding out the weight of the skin relation to the fineness ratio.

**Goals**
The goals are to find out the exact plot of the graph regards to the Lift Coefficient, Cl and Drag Coefficient, Cd vs Angle of Attack, Lift Coefficient, Cl and Drag Coefficient, Cd, Lift Coefficient, Cl and Drag Coefficient, Cd and the calibration on error made to the calculations of the finding out weight of the skin.

**Problem**
Problems were discovered in terms of determining similarities or matching the graph plotting that had been produced. The graph of the Lift Coefficient, Cl, and Drag Coefficient, Cd versus Angle of Attack does not reflect the same characteristics as those shown in the Fundamental of Airship and Design Book. The weight per 1 metre square of skin weight discovered was therefore incorrect due to unknown materials that had not been recognised while in development, as well as the confusion in the volume statement.

**Decision**
The Graph of results mainly the Lift Coefficient, Cl and Drag Coefficient, Cd vs Angle of Attack were analysed again. Then, the calculation progress especially on the weight of the skin had to be restructured again from the base.

**Method**
The simulation team decided to step the progress or push the progress into numerous tasks by splitting the subtasks to each member of the group in order to obtain more data findings from Ansys Software by employing different Angles of Attack, A.O.A. The calculations section was then reorganised to look for the right weight per 1 metre square of skin weight, which was required in the simulation calculation. The weight per 1 metre square of skin was determined in two ways: by searching through a materials catalogue or by using Ansys software analysis to determine the Surface Area of Skin and its relationship to the overall weight of skin.

**Justifications**
The presentable findings would be in terms of the Ansys data, as well as a graph displaying the Lift Coefficient, Cl and Drag Coefficient, Cd against Angle of Attack, which is required. The estimated weight of the skin may then be calculated by calculating the actual weight per 1 metre square of skin weight and multiplying it by the length, a.

**Impact**
The primary issues encountered before in determining the coefficients can be resolved by assigning the responsibility to everyone involved in the Ansys software study. The computations can be calibrated in terms of skin weight as well as measuring length, a.

**Next Step**
To plan out for the another calibration on the coefficients result through Ansys software analysisand also the calculation with the help of the lecturer if needed.
