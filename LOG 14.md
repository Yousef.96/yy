
- Yousef Abdulla Saleh 195567
- Subsystem:Simulation
- Team:7

**- Agenda**
To present all finalised mathematical data to Dr. Salah in order to verify for errors as well as final modifications. Then, verify the weight of all the systems connected to the airship.

**-Goals**
To finalise the helium weight after susbsytem weight check.

**-Problem**
have theproblem in the susbystem weight check.

**-Decision**
All of the system weight, save the cargo, was attached to the airship. The airship was then moored and let to float with the weight balance.

**-Method**
The formula and the centre gravity technique were used to determine the exact gravity point on the airship.

**-Justifications**
To get an accurate estimate of the complete subsystem weight, the theoretical weight analysis may be compared.

**-Impact**
The right helium weight v=can be determined in order to fill up in the flying day.
 
**-Next Step**
Fly the airship on the same day, double-checking the lift statistics and helium weight.
