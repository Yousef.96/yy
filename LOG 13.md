
- Yousef Abdulla Saleh 195567
- Subsystem:Simulation
- Team:7

**- Agenda**
Finalise the weight of helium needed through google spreadsheets.

**- Goals**
To begin working on the report progress under the simulation team and to mathematically finalise the findings of the data for the lift data.

**- Problem**
The susbystem weight had made some changes in which may cause the final helium weight to change.

**- Decision**
Collect the most recent subsystem weight to update the lift and helium weight data.

**- Method**
After the final weighing check using the weighing balance, the susbystem weight was updated.

**- Justifications**
To obtain the correct estimation the lift-weight data.

**- Impact**
The new helium weight as well as the amount of helium required to be filled may be approximated.



**- Next Step**
Final cross check for the airship flying week.
