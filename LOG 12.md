- Yousef Abdulla Saleh 195567
- Subsystem:Simulation
- Team:7

**- Agenda**
To complete the lift and weight per skin segment plots

**-Goals**
To produce the right statistics for the lift and the lift-weight per skin. Then, in order to make the modulation, aato must finalise the quantity of helium required to fill up.

**- Problem**
The weight of helium needed was getting bigger when the weight of the subsytem decreased.

**-Decision**
The internet resources were used to determine the amount of helium required for the balloon to carry the weight. The results were then compared to engineering-based estimates on the volume of the airship performed in excel spreadsheets using the airship design requirements.

**-Method**
Used the engineering volume formula of the airship.

**-Justifications**
To be able to obtain an approximation of the volume required in order to estimate the lift section.

**-Impact**
The right estimation on the lift and the volume of helium can be done.

**-Next Step**
To do calibrations on the weight of the helium as well to come out with right data on the airship's lift.
