- Yousef Abdulla Saleh 195567
- Subsystem:Simulation
- Team:7

**Agenda**
- To present to Dr. Salah and obtain feedback on the produced plots in order to build the optimal plots for the lift vs length graphs for the revised subsystem weight.

**Goals**
- To get a perfect match of the plot in terms of the clean weight of the subsystem, eliminate the payload weight from the system and then calculate the lift.

**Problem**
- The plots were right, but there was some uncertainty since the lift response was impossible, resulting in a length that was considerably longer than expected based on statistical experimental data analysis. Confusion occurred on the lift unit, which is supposed to be in Kg or Newton units.

**Decision** 
- Revisit the analyis data on the lift and the lenght datas.

**Method**
- To determine the unit to consider for the lift, use the equation derivation in therm.

**Justifications**
- The appropriate data plots on the Lift vs Lengthan would be generated, and the unit utilised in the analysis would be established.

**Impact**
- The whole gross weight may be calculated, and the terms of the gross lift can be calculated using the analytical mathematical model.

**Next Step**
- The correction will have to do unless there is the mistakes in the plots.
